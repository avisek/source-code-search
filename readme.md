This module can be attached to any existing Git Server hosting code repositories. Once attached it provides full text search to the entire bulk of source code across repositories on the server.
